all: createitemgraph alteredrw RandomWalk ci2

createitemgraph:
	$(MAKE) -C Snap-2.3/myApps/create-itemgraph
	
alteredrw:
	$(MAKE) -C Snap-2.3/myApps/altered-rw
	
RandomWalk:
	$(MAKE) -C Snap-2.3/myApps/RandomWalk
	
asciiize:
	$(MAKE) -C tools
	
ci2:
	$(MAKE) -C Snap-2.3/myApps/ci2
	
snap:
	$(MAKE) -C Snap-2.3
	
snapclean:
	$(MAKE) clean -C Snap-2.3
	
clean:
	$(MAKE) clean -C Snap-2.3/myApps/MyApp
	
test:
	$(MAKE) -C Snap-2.3/myApps/test

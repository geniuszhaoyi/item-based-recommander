import os,sys
import random
import time
import threading
import readline
import copy
import logging

# restriction 2
import snap

# restriction 3
from gensim import corpora, models, similarities

# restriction 5
from pylab import *

# restriction 6
class EnvironException(Exception):
    def __init__(self):
        Exception.__init__(self)
    def __str__(self):
        return repr('Some of environment verible is occupied. ')
f=open('setup/environ.txt')
ls=f.readlines()
f.close()
for l in ls:
    try:
        x=l.split('=')
        y=os.environ[x[0]]
        raise EnvironException()
    except KeyError:
        y=''


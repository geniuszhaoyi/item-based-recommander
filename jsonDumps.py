import os,sys
import json
import random

def choose(FILE_NUMBER):
    return int(random.random()*FILE_NUMBER)

def main():
    FILE_NUMBER = 5
    dataPath="tmp/filtedDataset.txt"
    jsonPath="tmp/jsonList.txt"

    for i in range(1,len(sys.argv)):
        x=sys.argv[i]
        if x[0]=='-':
            y=x[1]
            z=x[2:]
            if y=='d': dataPath=z
            if y=='j': jsonPath=z
            if y=='s': random.seed(1)
    
    f=open(dataPath)
    
    fout=open(jsonPath,'w')
    rates=[]
    rate={}
    tempstr=''
    strsize=0
    lstsize=0
    mark=0
    i=0
    while True:
        l=f.readline()
        if not l: break
        tempstr+=l
        strsize+=len(l)
        if strsize-lstsize>=10*1000**2: 
            lstsize=strsize
            if strsize/1000==0: print "%6d B" % strsize
            elif strsize/(1000**2)==0: print "%6.2f KB" % (float(strsize)/(1000.0**1))
            elif strsize/(1000**3)==0: print "%6.2f MB" % (float(strsize)/(1000.0**2))
            else: print "%6.3f GB" % (float(strsize)/(1000.0**3))
        if l.startswith('product/productId:'): rate['itemId']=l.split()[1]
        if l.startswith('review/userId:'): rate['userId']=l.split()[1]
        if l.startswith('review/profileName:'): rate['profileName']=l.replace('review/profileName: ','').strip('\n')
        if l.startswith('review/helpfulness:'): rate['helpfulness']=l.split()[1]
        if l.startswith('review/score:'): rate['score']=l.split()[1]
        if l.startswith('review/time:'): rate['time']=l.split()[1]
        if l.startswith('review/summary:'): rate['summary']=l.replace('review/summary: ','').replace('\n','').strip('\n')
        if l.startswith('review/text:'): rate['text']=l.replace('review/text: ','').replace('\n','').strip('\n')
        if l=='\n':
            rate['number']=choose(FILE_NUMBER)
            fout.write(json.dumps(rate)+'\n')
            i+=1
            #rates.append(rate)
            tempstr=''
            rate={}
    
    print i
    
    fout.close()
    
    f.close()
    
if __name__=='__main__':
    main()

import os,sys
import json
import random

def main():
    FILE_NUMBER=5
    jsonPath="tmp/jsonList.txt"
    trainpath='./tmp/foods.train.txt'
    testpath='./tmp/foods.test.txt'
    
    trainset=[0,1,2,3]
    testset=[4]
    
    if len(sys.argv)==3:
        trainset=[int(i) for i in sys.argv[1].split(',')]
        testset=[int(i) for i in sys.argv[2].split(',')]
        
    if len(sys.argv)==2:
        trainset=[int(i) for i in sys.argv[1].split('-')[0].split(',')]
        testset=[int(i) for i in sys.argv[1].split('-')[1].split(',')]
    
    f=open(jsonPath)

    ftrain=open(trainpath,'w')
    ftest=open(testpath,'w')
    
    i=0
    while True:
        if i%10000==0: print i
        i+=1
        l=f.readline()
        if not l: break
        d=json.loads(l)
        if d['number'] in trainset: ftrain.write(l)
        if d['number'] in testset: ftest.write(l)
    
    ftrain.close()
    ftest.close()
    
    f.close()
    
if __name__=='__main__':
    main()

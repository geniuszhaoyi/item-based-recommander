# file = go.sh
# author = zhaoyi

for i in $(cat environ.conf); do export $i; done

cvi='0,1,2,3-4'

python jsonLoads.py $cvi

python prelda-texts.py
python prelda-corpus.py
python lda.py
python getJson.py

python ./tools/convertTest.py -itmp/foods.test.txt

./Snap-2.3/myApps/ci2/main.out -t$RIB_THRESHOLD -f200 -w4 < ./tmp/foodstopics.txt
#./Snap-2.3/myApps/create-itemgraph/MyApp.out -t0.$RIB_THRESHOLD -f200 -w4 < ./tmp/foodstopics.txt

cp tmp/cpp.test.graph tmp/rw.graph
mv tmp/cpp.test.graph tmp/cpp.test.$RIB_THRESHOLD.graph

python altered-rw.py
cp ./tmp/rwed-globe.txt ./tmp/rwed-$RIB_THRESHOLD.$cvi.$RIB_STOPRATE-$RIB_ITTIME.txt

python getPredicts.py
mv tmp/result.txt tmp/result.$RIB_THRESHOLD.txt

str=""
for i in $(seq 20)
do
    python ./tools/cut.py -itmp/result.$RIB_THRESHOLD.txt -otmp/pcf/cm$i.txt -l$i
    python evaluation/Precision_Recall_F1.py -rtmp/pcf/cm$i.txt -l$i > tmp/pcf/$i
    str=${str}" tmp/pcf/"${i}
done

cat $str > tmp/pcf.$RIB_THRESHOLD.$RIB_STOPRATE.$RIB_ITTIME.txt


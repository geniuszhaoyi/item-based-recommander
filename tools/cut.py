import os,sys

def main():
    RecMax=5
    Inputfile='result.txt'
    Outputfile='result-m5.txt'
    
    for i in range(1,len(sys.argv)):
        x=sys.argv[i]
        if x[0]=='-':
            y=x[1].lower()
            z=x[2:]
            if y=='i': Inputfile=z
            if y=='o': Outputfile=z
            if y=='l': RecMax=int(z)
    
    f=open(Inputfile)
    ls=f.readlines()
    f.close()
    
    f=open(Outputfile,'w')
    
    for l in ls:
        x=l.split()
        f.write(x[0])
        x=x[1:RecMax+1]
        for xi in x:
            f.write(' '+xi)
        f.write('\n')

if __name__=='__main__':
    main()


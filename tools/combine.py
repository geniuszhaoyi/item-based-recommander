import os,sys

def main():
    RecMax=100

    f1=open(sys.argv[1])
    f2=open(sys.argv[2])
    fo=open(sys.argv[3],'w')
    
    ls1=f1.readlines()
    ls2=f2.readlines()
    users={}
    for l in ls1:
        x=l.split()
        users[x[0]]=x[1:]
        
    for l in ls2:
        x=l.split()
        users[x[0]]+=x[1:]
        users[x[0]]=users[x[0]][:RecMax]
    
    for (k,v) in users.items():
        fo.write(k)
        for vi in v:
            fo.write(' '+vi)
        fo.write('\n')
    
if __name__=='__main__':
    main()

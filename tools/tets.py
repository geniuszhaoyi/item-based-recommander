import os,sys

import json

def main():
    target='AVQIRN6E7J7UA'

    # Read in Json
    f=open('tmp/json.txt')
    strall=f.read()
    f.close()
    ds=json.loads(strall)
    
    for d in ds:
        itemId=d['ItemId']
        review=d['review']
        for r in review:
            userId=r['userId']
            if userId==target:
                print 'itemId: ',itemId
                print 'profileName: ',r['profileName']
                print 'helpfulness: ',r['helpfulness']
                print 'score: ',r['score']
                print 'time: ',r['time']
                print 'summary: ',r['summary']
                print 'text: ',r['text']
                print

if __name__=='__main__':
    main()

import os,sys
import json

def main():
    path='tmp/foods.train.txt'
    if len(sys.argv)==2: path=sys.argv[1]
    f=open(path)
    
    users={}
    items={}
    score=0
    
    while True:
        l=f.readline()
        if not l: break
        d=json.loads(l)
        items[d['itemId']]=1
        users[d['userId']]=1
        score+=1
    
    print 'Number of users: ',len(users)
    print 'Number of items: ',len(items)
    print 'Number of scores:',score
    
if __name__=='__main__':
    main()

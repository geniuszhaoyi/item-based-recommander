import os
import sys

import snap
from pylab import *

# Step
step=10

ptype='asdd'

inpath="./tmp/test.graph"

# List
count=[]

for i in range(1,len(sys.argv)):
    x=sys.argv[i]
    if x[0]=='-':
        y=x[1]
        z=x[2:]
        if y=='s': step=int(z)
        if y=='p': ptype=z
        if y=='i': inpath=z

# Read in Graph
FIn = snap.TFIn(inpath)
g=snap.TUNGraph.Load(FIn)

# Get max value of Node's Degree
nid=snap.GetMxDegNId(g)
ni=g.GetNI(nid)
max=ni.GetDeg()

for i in range((max-1)/step+10):
    count.append(0)

for NI in g.Nodes():
    x=NI.GetDeg()
    count[x/step]+=1

x=[]
y=[]
for i in range((max-1)/step+1):
    print str(i+1)+' '+str(count[i])
    x.append(i+1)
    y.append(count[i])

fig = plt.figure("Figure of "+inpath)
ax = fig.add_subplot(111)
if ptype=='log':
    ax.set_xscale("log")
    ax.set_yscale("log")
ax.plot(x, y, 'o-')
plt.draw()
plt.show()

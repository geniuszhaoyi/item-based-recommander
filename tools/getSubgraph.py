import os,sys
import json

import snap

def dictmerge(a, b):
    for bi,v in b.items():
        if bi not in a: a[bi]=v

def getShortestPath(g, a, b):
    ans={}
    
    visited={a:-1}
    
    s=0
    bfslist=[a]
    while s!=len(bfslist):
        #print s, bfslist
        if bfslist[s]==b: break
        ni=g.GetNI(bfslist[s])
        n=ni.GetDeg()
        for i in range(n):
            thisnbr=ni.GetNbrNId(i)
            if thisnbr not in visited:
                bfslist.append(thisnbr)
                visited[thisnbr]=bfslist[s]
        s+=1
    
    if s!=len(bfslist):
        this=b
        #print this
        while this>=0:
            ans[this]=1
            this=visited[this]
    else:
        ans[a]=1
        ans[b]=1
    #print 'END getShortestPath'
    return ans

def GetSubGraphWithConnection(g, IdV):
    ans={}
    n=len(IdV)
    for i in xrange(n):
        print i
        for j in xrange(i+1,n):
            p=getShortestPath(g, IdV[i],IdV[j])
            dictmerge(ans,p)
    
    NIdV=snap.TIntV()
    for (a,v) in ans.items():
        NIdV.Add(a)
    
    return snap.GetSubGraph(g, NIdV)

def main():
    eps=0.00001
    target='AVQIRN6E7J7UA'

    for i in range(1,len(sys.argv)):
        x=sys.argv[i]
        if x[0]=='-':
            y=x[1].lower()
            z=x[2:]
            if y=='t': target=z

    # Read in Graph
    FIn = snap.TFIn('./tmp/rw.graph')
    g=snap.TUNGraph.Load(FIn)

    # Read in Json
    f=open('tmp/json.txt')
    strall=f.read()
    f.close()
    ds=json.loads(strall)
    
    NIdName = snap.TIntStrH()
    for i in range(100000):
        NIdName[i]=''

    IdV = snap.TIntV()
    for d in ds:
        itemId=d['ItemId']
        review=d['review']
        for r in review:
            userId=r['userId']
            try:
                score=float(r['score'])
            except:
                continue
            if score-eps<=2.0: continue
            if userId==target:
                IdV.Add(itemId)
                NIdName[itemId]=str(itemId)
    
    subg = GetSubGraphWithConnection(g, IdV)
    
    snap.DrawGViz(subg, snap.gvlDot, "tmp/subg.png", 'userId: '+target, NIdName)
    
if __name__=='__main__':
    main()

import os,sys
import json

def main():
    InputFile = './tmp/foods.test.txt'
    OutputFile = './tmp/test.txt'
    
    for i in range(1,len(sys.argv)):
        x=sys.argv[i]
        if x[0]=='-':
            y=x[1].lower()
            z=x[2:]
            if y=='i': Inputfile=z    
            if y=='o': Outputfile=z    
    
    f=open(InputFile)
    
    users={}
    while True:
        l=f.readline()
        if not l: break
        d=json.loads(l)
        if d['userId'] not in users: users[d['userId']]=[]
        users[d['userId']].append(d['itemId'])
        
    fout=open(OutputFile,'w')
    for (k,v) in users.items():
        fout.write(k)
        for vi in v:
            fout.write(' '+vi)
        fout.write('\n')
    fout.close()
    
if __name__=='__main__':
    main()

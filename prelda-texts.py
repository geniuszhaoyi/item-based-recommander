# Usage : Reading information from FOOD.txt 

import sys
import os
import shutil
import logging
from gensim import corpora, models, similarities

import json


def main():
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
    
    ifs = ['.',',','</br>','-','*','<br>',';','!','?','&amp','(',')',"'",'"','/',"<br"]

    NUM_TOPICS = 200
    MinWordFreq = 0
    BufMax = 100000000 # 100M
    
    Inputfile = './tmp/foods.train.txt'
    Stoplist = 'Stoplist.ini'
    StorePath_Texts = 'tmp/Texts.txt'
    StoreJsonPath='tmp/json.txt'
    StoreItemlistPath='tmp/itemlist.txt'
    
    for i in range(1,len(sys.argv)):
        x=sys.argv[i]
        if x[0]=='-':
            y=x[1].lower()
            z=x[2:]
            if y=='i': Inputfile=z
            if y=='s': Stoplist=z
            if y=='x': StorePath_Texts=z
            if y=='j': StoreJsonPath=z
            if y=='l': StoreItemlistPath=z
        # Read in Stoplist for removing common words
    f=open(Stoplist)
    stoplist=set(f.read().split())
    f.close()
    
    f=open(Inputfile)
    fout=open(StorePath_Texts,'w')
    itemlist=[]
    itemdict={}
    i=0
    bufstr=''
    buflen=0
    while True:
        l=f.readline()
        if not l: break
        if i%10000==0: print i,'/',5439614
        i+=1
        ds=json.loads(l)
        itemId=ds['itemId']
        text=ds['summary']+' '+ds['text']
        
        if i==1:
            itemlist.append(itemId)
            itemdict[itemId]=1
        if i!=1 and itemId not in itemdict:
            itemlist.append(itemId)
            itemdict[itemId]=1
            for ifi in ifs:
                bufstr=bufstr.replace(ifi,' ')
            bufstr+='\n'
            fout.write(bufstr)
            bufstr=''
            
        bufstr+=text+' '
    fout.write(bufstr)
    
    f.close()
    fout.close()
    print 'build Documents finished'
    print 'save texts finished'
    
    bfstr=''
    illen=len(itemlist)
    for i in itemlist:
        bfstr+=i+'\n'
    f=open(StoreItemlistPath,'w')
    f.write(bfstr)
    f.close()
    print 'save itemlist finished'

if __name__=='__main__':
    main()

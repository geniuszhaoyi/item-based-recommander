# file = altered-rw.py
# author = zhaoyi

import os,sys

import snap
import json

def main():
    NUM_ITEMS=13630800
    
    f=open('./tmp/itemlist.txt');
    NUM_ITEMS=len(f.readlines())-1
    f.close()
    print 'item number: ', NUM_ITEMS
    
    f=open('./tmp/json.txt')
    strall=f.read()
    f.close()
    ds=json.loads(strall)
    
    nodesum=0
    newprh=[0 for i in xrange(NUM_ITEMS+1)]
    totle=0
    dslen=len(ds)
    for d in ds:
        totle+=1
        itemId=d['itemId']
        review=d['review']
        l=len(review)
        newprh[itemId]+=l
        nodesum+=l

    fout=open("./tmp/rw.inpif","w")
    fout.write(str(NUM_ITEMS)+'\n')
    for x in range(len(newprh)):
        fout.write(str(x)+' '+str(float(newprh[x])/float(nodesum))+'\n')
    fout.close()
    
    print 'Begin Random-walk...'
    os.system('./Snap-2.3/myApps/RandomWalk/RandomWalk.out ./tmp/rw.graph ./tmp/rw.inpif ./tmp/rw.onpif '+os.environ["RIB_STOPRATE"]+' '+os.environ['RIB_ITTIME'])
    print 'End Random-walk...Saving Results'
    
    os.system('mv ./tmp/rw.onpif ./tmp/rwed-globe.txt')
    
if __name__=='__main__':
    main()

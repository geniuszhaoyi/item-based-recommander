import os,sys

def percent(flt):
    return str(flt*100)+'%'
def percent(flt, n):
    return str(int(flt*1000)/10.0)+'%'

def list2dict(lst):
    d={}
    for l in lst:
        d[l]=1
    return d

def main():
    RecMax=5
    ResultPath='tmp/result.txt'
    ConvertedTestPath='tmp/test.txt'
    
    for i in range(1,len(sys.argv)):
        x=sys.argv[i]
        if x[0]=='-':
            y=x[1].lower()
            z=x[2:]
            if y=='r': ResultPath=z
            if y=='t': ConvertedTestPath=z
            if y=='l': RecMax=int(z)
    
    a=0.0 
    ab=0.0
    ac=0.0
    abp=0
        
    f=open(ConvertedTestPath)
    ls=f.readlines()
    f.close()
    
    user_test={}
    for l in ls:
        x=l.split()
        user_test[x[0]]=list2dict(x[1:])
        ac+=len(x[1:])
    
    f=open(ResultPath)
    ls=f.readlines()
    f.close()
    
    user_result_count=[0 for i in range(RecMax+1)]
    user_result={}
    for l in ls:
        x=l.split()
        if x[0] not in user_test: continue
        ab+=RecMax #len(x[1:])
        abp+=len(x[1:RecMax+1])
        user_result[x[0]]=x[1:RecMax+1]
        user_result_count[len(x[1:RecMax+1])]+=1
        for i in x[1:RecMax+1]:
            if i in user_test[x[0]]: a+=1

    sys.stderr.write('a:'+str(a)+' ab:'+str(ab)+' ac'+str(ac)+'\n')


    if a!=0: print a/ab,a/ac,2.0*a/ab*a/ac/(a/ab+a/ac),
    else: print '0.0 0.0 0.0',
    print int(a),int(ab),int(ac),
    print abp,
    print float(user_result_count[RecMax])/float(sum(user_result_count)),
    print float(sum([i*user_result_count[i] for i in range(RecMax+1)]))/float(sum(user_result_count)),
    print user_result_count[RecMax],sum(user_result_count),
    
    Q=0.0
    mrrsum=0.0
    for user,items in user_test.items():
        if user not in user_result: continue
        x=user_result[user]
        #sys.stderr.write(user+'\n')
        #sys.stderr.write(str(x)+'\n')
        for i in items:
            #sys.stderr.write(i+'\n')
            if i in x:
                mrrsum+=1.0/float(x.index(i)+1)
            Q+=1
    
    print mrrsum/Q,
    
    print ' '
	
    sys.stderr.write('Precision:'+percent(a/ab,3)+' Recall:'+percent(a/ac,3)+' F1:'+percent(2.0*a/ab*a/ac/(a/ab+a/ac),3)+' ')
    sys.stderr.write('MRR:'+percent(mrrsum/Q,3)+'\n')
    
if __name__=='__main__':
    main()

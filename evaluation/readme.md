#Precision#
准确率 P = 系统检索到的相关文件 / 系统所有检索到的文件总数

#Recall#
召回率 R = 系统检索到的相关文件 / 系统所有相关的文件总数

#F1#
F1 = 2 * P * R / (P + R)

#Spearman's rank correlation coefficient#
![pic](http://www.forkosh.com/mathtex.cgi?\rho=1-\frac{6\sum{d_i^2}}{n^3-n})


#Reference#
http://blog.csdn.net/wangzhiqing3/article/details/9058523
http://en.wikipedia.org/wiki/Spearman's_rank_correlation_coefficient


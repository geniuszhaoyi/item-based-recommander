# file = go.sh
# author = zhaoyi

for i in $(cat environ.conf); do export $i; done

cvi='0,1,2,3-4'

#python jsonLoads.py $cvi

#python prelda-texts.py
#python prelda-corpus.py
#python lda.py
#python getJson.py
echo 'FoodCal finished' > status.txt

#python ./tools/convertTest.py -itmp/foods.test.txt

threshold=996

./Snap-2.3/myApps/ci2/main.out -t0.$threshold -f200 -w4 < ./tmp/foodstopics.txt
#./Snap-2.3/myApps/create-itemgraph/MyApp.out -t0.$threshold -f200 -w4 < ./tmp/foodstopics.txt

cp tmp/cpp.test.graph tmp/rw.graph
mv tmp/cpp.test.graph tmp/cpp.test.$threshold.graph

for stoprate in $(cat 'tools/stoprate.for')
do
    export RIB_STOPRATE=$stoprate
    
    python altered-rw.py
    cp ./tmp/rwed-globe.txt ./tmp/rwed-$threshold.$cvi.$RIB_STOPRATE-$RIB_ITTIME.txt

    python getPredicts.py
    mv tmp/result.txt tmp/result.$threshold.txt

    str=""
    for i in $(seq 20)
    do
        python ./tools/cut.py -itmp/result.$threshold.txt -otmp/pcf/cm$i.txt -l$i
        python evaluation/Precision_Recall_F1.py -rtmp/pcf/cm$i.txt -l$i > tmp/pcf/$i
        str=${str}" tmp/pcf/"${i}
    done
    
    #echo $str
    cat $str > tmp/pcf.$threshold.$cvi.$RIB_STOPRATE.txt

done

echo 'ALL finished' > status.txt


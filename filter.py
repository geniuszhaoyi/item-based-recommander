import os,sys

def main():
    min_Raters=5
    Input_File=os.environ["RIB_RAW_DATA_PATH"]
    Output_File='tmp/filtedDataset.txt'
    
    # If dataset == Clothing, isclothing=2, otherwise isclothing=0
    isclothing=0

    for i in range(1,len(sys.argv)):
        x=sys.argv[i]
        if x[0]=='-':
            y=x[1].lower()
            z=x[2:]
            if y=='m': min_Raters=int(z)
            if y=='i': Input_File=z
            if y=='o': Output_File=z
    
    f=open(Input_File)
    ls=f.readlines()
    f.close()
    
    users={}
    for l in ls:
        if l=='\n': continue
        x=l.split()
        key='review/userId:'
        if x[0]==key:
            if x[1] not in users: users[x[1]]=0
            users[x[1]]+=1
    
    fout=open(Output_File,'w')
    
    lenls=len(ls)
    for i in xrange(lenls):
        if i%100000==0: print i,'/',lenls
        if ls[i].startswith('product/productId:'):
            x=ls[i+1+isclothing].split()
            #print x[0],x[1]
            if users[x[1]]<min_Raters: continue
            bufferstr=''
            for j in range(i,i+8+isclothing):
                if ls[j].startswith('product/title:') or ls[j].startswith('product/price:'): continue
                bufferstr+=ls[j]
            fout.write(bufferstr+'\n')
    
    fout.close()
    
if __name__=='__main__':
    main()

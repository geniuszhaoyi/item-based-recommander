# Usage : Reading information from FOOD.txt 

import sys
import os
import shutil
import logging
from gensim import corpora, models, similarities

import json


def main():
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

    NUM_TOPICS=200
    MinWordFreq=0
    
    StoreTopicsPath = './tmp/foodstopics.txt'
    CorpusPath = 'tmp/corpus.lda-c'
    VocabPath = 'tmp/corpus.lda-c.vocab'

    corpus=corpora.BleiCorpus(CorpusPath, fname_vocab=VocabPath)
    print 'Load corpus Finished'
    
    # train model
    lda = models.LdaModel(corpus, num_topics=200)
    print 'LDA finished'
    
    bfstr=''
    it=0
    for c in corpus:
        if it%10000==0: print it
        it+=1
        x=[0.0 for i in range(NUM_TOPICS)]
        doc_lda = lda[c]
        for d in doc_lda:
            x[d[0]]=d[1]
        for i in range(NUM_TOPICS):
            bfstr+=str(x[i])+' '
        bfstr+='\n'
    f=open(StoreTopicsPath,'w')
    f.write(bfstr)
    f.close()
    
    print 'All finished! \n'
    
if __name__=='__main__':
    main()

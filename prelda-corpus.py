# Usage : Reading information from FOOD.txt 

import sys
import os
import shutil
import logging
from gensim import corpora, models, similarities

import json


def main():
    #logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

    NUM_TOPICS=200
    MinWordFreq=0
    
    Inputfile = './tmp/foods.train.txt'
    Stoplist = 'Stoplist.ini'
    StorePath_Texts = 'tmp/Texts.txt'
    StoreJsonPath='tmp/json.txt'
    StoreItemlistPath='tmp/itemlist.txt'
    CorpusPath = 'tmp/corpus.lda-c'
    
    for i in range(1,len(sys.argv)):
        x=sys.argv[i]
        if x[0]=='-':
            y=x[1].lower()
            z=x[2:]
            if y=='i': Inputfile=z
            if y=='s': Stoplist=z
            if y=='x': StorePath_Texts=z
            if y=='j': StoreJsonPath=z
            if y=='l': StoreItemlistPath=z
        # Read in Stoplist for removing common words

    f=open(StorePath_Texts)
    
    dictionary = corpora.Dictionary(documents=None)
    corpus=[]
    lastsize=0
    readsize=0
    while True:
        l=f.readline()
        if not l: break
        readsize+=len(l)
        if readsize-lastsize>10000000:  #10M
            print readsize/1000000,'M'
            lastsize=readsize
        x=l.split()
        dictionary.add_documents([x])
        corpus.append(dictionary.doc2bow(x))
    print 'build Corpus finished'
    
    corpora.BleiCorpus.serialize(CorpusPath, corpus)
    
    print sys.getsizeof(corpus)
    print sys.getsizeof(dictionary)
    
if __name__=='__main__':
    main()

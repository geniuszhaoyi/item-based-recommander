# Require: 

1. gcc 4.8.2
2. Snap.py Download at http://snap.stanford.edu/snappy/index.html
3. SNAP for C++, automatic downloaded. 
4. gensim
5. matplotlib
6. Make sure you don't have environment variables which start with 'RIB_'. 
7. Network access
8. RAM at least 8G for dataset foods & clothing, 32G for dataset movies. 
9. Linux

2,4,5,6 will be checked in `setup.sh` .

# Install: 

`./setup.sh`

You need to specify the path of your Dataset. 
        
# Run: 

Run `./go.sh`, the file below will be created, if no exceptions occurred:

`tmp/pfc.A.B.C.txt`

*A* is for *threshold*, *B* is for *APR* and *C* is for *iterations*. 

Line 1 to line 20 are evaluations of the recommend-lists which length range from 1 to 20, respectively. 

Each line has several numbers, respecting the below: 

- Precision
- Recall
- F1
- hit number
- recommend number
- review number
- actual recommend number
- percentage of fully-recommended users
- average recommend-list length
- number of fully-recommended users
- number of users
- MRR

Separated by space. 


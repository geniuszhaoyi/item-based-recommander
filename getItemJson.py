import sys
import os
import shutil
import logging
from gensim import corpora, models, similarities

import json

def readall(path):
    ss=''
    f=open(path)
    ls=f.readlines()
    f.close()
    for l in ls:
        ss+=l
    return ss


def main():
    ss=readall(path='./tmp/json.txt')
    items=json.loads(ss)
    if len(sys.argv)!=1:
        for i in range(1,len(sys.argv)):
            getItem(items,int(sys.argv[i]))
        return
    while 1:
        try:
            line=raw_input()
            getItem(items,int(line))
        except EOFError:
            break
    print 'bye\n'

def getItem(items,ItemId):
    for i in items:
        if i['itemId']==ItemId:
            print json.dumps(i)

if __name__=='__main__':
    main()

# file = go.sh
# author = zhaoyi

threshold=996

for i in $(cat environ.conf); do export $i; done

# 过滤掉评论数小于5的用户，输出是 ./tmp/filtedDataset.txt
#python ./tools/filter.py -i$RIB_RAW_DATA_PATH -m5

# 将过滤后的数据转换成Json格式，输出存在./tmp/文件夹下
#python tools/jsonDumps.py -dtmp/filtedDataset.txt

# 划分5-fold CV的训练集和测试集
for cvi in $(cat 'tools/cv.for')
do
    # 将Jason格式转换成TXT格式（将每组数据划分为训练集和测试集，保存）
    python tools/jsonLoads.py $cvi

    # 处理Review信息，执行LDA，保存对每个物品的Topic分布
    python FoodCal.py
    echo 'FoodCal finished' > status.txt

    # 建立物品和物品之间的相似度矩阵（Snap格式，稀疏矩阵）
    # t0.95 为相似度过滤采用的threshold
    ./Snap-2.3/myApps/create-itemgraph/MyApp.out -t0.$threshold -f200 -w4 < ./tmp/foodstopics.txt
    echo 'CreateGraph 1 finished' > status.txt

    # rw.graph
    # cpp.test.$threshold.graph
    cp tmp/cpp.test.graph tmp/rw.graph
    mv tmp/cpp.test.graph tmp/cpp.test.$threshold.graph

    # random walk
    python altered-rw.py
    echo 'altered-rw 1 finished' > status.txt

    # 得到对每个用户的推荐列表
    python getPredicts.py
    mv tmp/result.txt tmp/result.$threshold.txt
    echo 'getPredicts 1 finished' > status.txt

    # 将测试集的评分记录转换为以用户为单位的访问记录（每行为该用户访问过的所有物品）
    python ./tools/convertTest.py -itmp/foods.test.txt

    # pcf目录用于存储中间结果
    mkdir tmp/pcf
    str=""
    for i in $(seq 20) # 记录推荐数为1～20的推荐结果
    do
        # 修剪推荐列表长度为指定值（取值范围[1,20]）
        python ./tools/cut.py -itmp/result.$threshold.txt -otmp/pcf/cm$i.txt -l$i
        # 计算准确率和召回率（准确率的分母是用户数量乘以列表长度；召回率分母是foods.test.txt相应用户（行）的记录数）
        python evaluation/Precision_Recall_F1.py -rtmp/pcf/cm$i.txt -l$i > tmp/pcf/$i
        str=${str}" tmp/pcf/"${i}
    done

    cat $str > tmp/pcf.$cvi.txt

done

echo 'ALL finished' > status.txt


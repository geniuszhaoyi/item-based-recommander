/**
* file = Snap-2.3/myApps/altered-rw/altered-rw.cpp
* author = zhaoyi
*/

#include <stdio.h>
#include <time.h>

#include "Snap.h"
#include "../../../cJSON/cJSON.h"

#define SHOWTIME

#define STR_LEN 600000000

int NUM_ITEMS=74257;

char str[STR_LEN+1];

int main(void){
#ifdef SHOWTIME
time_t starter=clock();
#endif

    TFIn FIn("./tmp/cpp.test-0.95.graph");
    PUNGraph g1=TUNGraph::Load(FIn);
#ifdef SHOWTIME
printf("%f\n",(double)(clock()-starter)/(double)CLOCKS_PER_SEC);
starter=clock();
#endif

    FILE *f=fopen("./tmp/json.txt","r");
    int strlen=fread(str,STR_LEN,1,f);
    fclose(f);
#ifdef SHOWTIME
printf("%f\n",(double)(clock()-starter)/(double)CLOCKS_PER_SEC);
starter=clock();
#endif

    cJSON *root = cJSON_Parse(str);
#ifdef SHOWTIME
printf("%f\n",(double)(clock()-starter)/(double)CLOCKS_PER_SEC);
starter=clock();
#endif
    
    
    
    return 0;
}


/**
* file: matrix.h
* author: zhaoyi
*/

#include <string.h>

long counter1=0,counter2=0,counter3=0;

pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;

struct SG{
    int n,width;
    long size;
    char *mem;
}sg;

struct SG SGinitBare(int n){
    struct SG sg;
    sg.n=n;
    sg.width=(n+7)/8;
    sg.size=(long)(sg.width)*(sg.n);
    return sg;
}
struct SG SGinit(int n){
    struct SG sg=SGinitBare(n);
    sg.mem=(char*)malloc(sg.size);
    memset(sg.mem,0,sg.size);
    return sg;
}
void SGfree(struct SG sg){
    free(sg.mem);
}

struct SG SGload(struct SG *psg, char *path){
    struct SG sg;
    FILE *f;
    char pathi[strlen(path)+5];
    int itemcount;
    //char *pathi=(char*)malloc(sizeof(char)*(strlen(path)+5));
    
    strcpy(pathi,path);
    strcat(pathi,".ic");
    f=fopen(pathi,"r");
    fscanf(f,"%d",&itemcount);
    sg=SGinit(itemcount);
    fclose(f);
    
    f=fopen(path,"r");
    long ssize=fread(sg.mem,sg.size,1,f);
    if(ssize!=1){
        printf("!!! Error in reading! %d,%d\n",ssize,sg.size);
        return sg;
    }
    fclose(f);
    
    //free(pathi);
    if(psg!=NULL) *psg=sg;
    return sg;
}
void SGsave(struct SG sg, char *path){
    FILE *f;
    char pathi[strlen(path)+5];
    //char *pathi=(char*)malloc(sizeof(char)*(strlen(path)+5));
    
    f=fopen(path,"w");
    long ssize=fwrite(sg.mem,sg.size,1,f);
    if(ssize!=1){
        printf("!!! Error in reading! %d,%d\n",ssize,sg.size);
        return ;
    }
    fclose(f);

    strcpy(pathi,path);
    strcat(pathi,".ic");
    f=fopen(pathi,"w");
    fprintf(f,"%d",sg.n);
    fclose(f);
    
    return ;
}

void SGaddEdge(struct SG sg, int s, int t){
    long id=(long)s*sg.width+t/8;
    long ib=t%8;
    pthread_mutex_lock(&mutex);
    *(sg.mem+id)|=(char)1<<ib;
    counter1+=1;
    pthread_mutex_unlock(&mutex);
}
void SGaddBedge(struct SG sg, int s, int t){
    SGaddEdge(sg,s,t);
    SGaddEdge(sg,t,s);
}

char SGgetEdge(struct SG sg, int s, int t){
    long id=(long)s*sg.width+t/8;
    long ib=t%8;
    return (*(sg.mem+id)>>ib)&1;
}


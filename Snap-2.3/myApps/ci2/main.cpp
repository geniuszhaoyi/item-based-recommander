/**
* file = Snap-2.3/myApps/create-itemgraph/MyApp.cpp
* author = zhaoyi
*/

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <math.h>

#include <pthread.h>

#include "Snap.h"

#include "../matrix_h/matrix.h"

using namespace std;

#define STEP 200
#define FEATURES 500

vector<struct node*> ls;
PUNGraph g;

double threshold=0.9;
int workers=1;
int features=20;
int itemcount=-1;
int Ctrl=3;

struct node{
    double line[FEATURES];
};

int readLine(struct node *nn){
    int x=0;
    char ch, str[1024];
    while(scanf("%s",str)==1){
        nn->line[x++]=atof(str);
        scanf("%c",&ch);
        if(ch=='\n') break;
        scanf("%c",&ch);
        if(ch=='\n') break;
    }
    return x;
}

double cmp(struct node *a,struct node *b){
    double top=0, b1=0,b2=0;
    for(int i=0;i<features;i++){
        top+=a->line[i]*b->line[i];
        b1+=a->line[i]*a->line[i];
        b2+=b->line[i]*b->line[i];
    }
    return top/sqrt(b1*b2);
}

struct tothread{
    int s,t;
};

void *thr_fn(void *arg){
    struct tothread tt=*((struct tothread*)arg);
    
    for(int i=tt.s;i<tt.t;i++){
        if(i%STEP==0) printf("[] %7d/%-7d\t%lf\%\n",i,tt.t,100.0*(double)(i-tt.s)/(double)(tt.t-tt.s));
        for(int j=i+1;j<itemcount;j++){
            double weight=cmp(ls[i],ls[j]);
            if(weight>threshold){
                pthread_mutex_lock(&mutex);
                g->AddEdge(i,j);
                counter3+=1;
                //SGaddEdge(sg,i,j);
                pthread_mutex_unlock(&mutex);
            }
        }
    }
    
    return NULL;
}

void cut(int *x){
    for(int i=0;i<=workers;i++){
        double y=1.0-sqrt(((double)workers-(double)i)/(double)workers);
        x[i]=(int)(y*itemcount);
    }
}

int main(int args, char *argv[]){
    for(int i=1;i<args;i++) if(argv[i][0]=='-'){
        if(argv[i][1]=='f') features=atoi(argv[i]+2);
        if(argv[i][1]=='t') threshold=atof(argv[i]+2);
        if(argv[i][1]=='w') workers=atoi(argv[i]+2);
        if(argv[i][1]=='c') Ctrl=atoi(argv[i]+2);
        if(argv[i][1]=='i') itemcount=atoi(argv[i]+2);
    }
    
    if((Ctrl&1)==1){
        printf("Creating snap file... \n");
        
        struct node *newnode=(struct node*)malloc(sizeof(struct node));
        
        int i=0;
        while(readLine(newnode)>0){
            ls.push_back(newnode);
            newnode=(struct node*)malloc(sizeof(struct node));
            i++;
        }
        if(itemcount<0) itemcount=i;
        printf("%d\n",itemcount);

        // Create new undirected
        g = TUNGraph::New();
        // Add nodes to graph
        for(int i=0;i<itemcount;i++){
            if(i%10000==0) printf("[AddNode] %d / %d\n",i,itemcount);
            g->AddNode(i);
        }
        //MOVED

        sg=SGinit(itemcount);

        int x[workers+1];
        cut(x);
        
        pthread_t ps[workers];
        
        for(int i=0;i<workers;i++){
            tothread *tt=(tothread*)malloc(sizeof(tothread));
            tt->s=x[i];
            tt->t=x[i+1];
            pthread_create(ps+i,NULL,thr_fn,(void*)tt);
        }
        
        for(int i=0;i<workers;i++){
            pthread_join(ps[i],NULL);
        }
        
        //SGaddEdge(sg,2135,9432);
        
        /*FILE *f=fopen("tmp/graph.matrix","w");
        long ssize=fwrite(sg.mem,sg.size,1,f);
        if(ssize!=1) printf("Error in writing! %d,%d",ssize,sg.size);
        fclose(f);
        //return 0;*/
        
        //SGsave(sg,"tmp/graph.matrix");
        //SGfree(sg);
    }
    
    if(((Ctrl>>1)&1)==1){

        //sg=SGinitBare(itemcount);
        //sg.mem=(char*)malloc(sg.width);
        
        //FILE *f=fopen("tmp/graph.matrix","r");
        for(int i=0;i<itemcount;i++){
            if(i%1000==0) printf("[AddEdge] %d / %d\n",i,itemcount);
            //long ssize=fread(sg.mem,sg.width,1,f);
            //if(ssize!=1) printf("!!! Error in reading! %d,%d\n",ssize,sg.size);
            for(int j=i+1;j<itemcount;j++){
                if(SGgetEdge(sg,i,j)==1) counter2+=1,g->AddEdge(i,j);
            }
        }
        
        printf("1: %d\t 2: %d\t 3: %d\n",counter1,counter2,counter3);

        printf("Saving to binary file... \n");
        TFOut FOut("./tmp/cpp.test.graph");
        g->Save(FOut);
        
        printf("------------------------------\nFinished\n");
    }
    
    return 0;
}



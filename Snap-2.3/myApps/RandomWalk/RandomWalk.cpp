/**
* file = Snap-2.3/myApps/RandomWalk/RandomWalk.cpp
* author = zhaoyi
*/

#include <stdio.h>
#include <string.h>
#include <time.h>

#include "Snap.h"

#include "../../../cJSON/cJSON.h"

#define SHOWTIME1

#define STR_LEN 600000000

char *graphpath="./tmp/rw.graph";
char *INPIFPath="./tmp/rw.inpif";
char *ONPIFPath="./tmp/rw.onpif";

double alpha=0.5;
int ittime=4;

double eps=0.0000001;
int NUM_ITEMS=74257;

char str[STR_LEN+1];

struct node{
    int key;
    double val;
    double newval;
};

int readNPIF(FILE *f, struct node **arr){
    int key;
    double val;
    fscanf(f,"%d",&NUM_ITEMS);
    *arr=(struct node*)malloc(NUM_ITEMS*sizeof(struct node));
    while(fscanf(f,"%d %lf",&key,&val)==2){
        struct node *p=*arr+key;
        p->val=val;
    }
    for(int i=0;i<NUM_ITEMS;i++) (*arr)[i].key=i;
    return NUM_ITEMS;
}
int saveNPIF(FILE *f, struct node *arr){
    fprintf(f,"%d\n",NUM_ITEMS);
    for(int i=0;i<NUM_ITEMS;i++){
        if(arr[i].val>eps) fprintf(f,"%d %.20lf\n",arr[i].key,arr[i].val);
    }
    return 0;
}
void setNPIF(struct node *arr, int key, double val){
    arr[key].key=key;
    arr[key].val=val;
}
double getNPIF(struct node *arr, int key){
    return arr[key].val;
}
void addNPIF(struct node *arr, int key, double addval){
    arr[key].newval+=addval;
}
void mvNPIFnew2old(struct node *arr){
    //printf("------------------------------------\n");
    for(int i=0;i<NUM_ITEMS;i++){
    //    if(i<40) printf("%d %lf\n",i,arr[i].newval);
        if(arr[i].newval>eps) arr[i].val=arr[i].newval;
        arr[i].newval=0.0;
    }
    //printf("------------------------------------\n");
}

void getRW_plan2(PUNGraph g,struct node *arr,int n){
    for(int it=0;it<n;it++){
        for(int key=0;key<NUM_ITEMS;key++) if(arr[key].val>eps){
            double myval=getNPIF(arr,key);
            TUNGraph::TNodeI ni=g->GetNI(key);
            addNPIF(arr,key,alpha*myval);
            int nns=ni.GetDeg();
            //printf(">>>%d\n",nns);
            for(int i=0;i<nns;i++){
                int nn=ni.GetNbrNId(i);
                addNPIF(arr,nn,(1.0-alpha)*myval/(double)nns);
            }
        }
        mvNPIFnew2old(arr);
    }
}

int main(int args, char *argv[]){
    if(args==4){
        graphpath=argv[1];
        INPIFPath=argv[2];
        ONPIFPath=argv[3];
    }    
    if(args==6){
        graphpath=argv[1];
        INPIFPath=argv[2];
        ONPIFPath=argv[3];
        alpha=atof(argv[4]);
        ittime=atoi(argv[5]);
    }
    if(args==3){
        alpha=atof(argv[2]);
        ittime=atoi(argv[3]);
    }
    
    printf("RandomWalk: %f\t%d\n",alpha,ittime);
#ifdef SHOWTIME
time_t starter=clock();
#endif
    
    TFIn FInG(graphpath);
    PUNGraph g=TUNGraph::Load(FInG);
#ifdef SHOWTIME
printf("1 %f\n",(double)(clock()-starter)/(double)CLOCKS_PER_SEC);
starter=clock();
#endif
    
    struct node *arr;
    FILE *FNPIF=fopen(INPIFPath,"r");
    NUM_ITEMS=readNPIF(FNPIF,&arr);
    fclose(FNPIF);
#ifdef SHOWTIME
printf("2 %f\n",(double)(clock()-starter)/(double)CLOCKS_PER_SEC);
starter=clock();
#endif
    
    getRW_plan2(g,arr,ittime);
#ifdef SHOWTIME
printf("3 %f\n",(double)(clock()-starter)/(double)CLOCKS_PER_SEC);
starter=clock();
#endif
    
    FNPIF=fopen(ONPIFPath,"w");
    saveNPIF(FNPIF,arr);
    fclose(FNPIF);
#ifdef SHOWTIME
printf("4 %f\n",(double)(clock()-starter)/(double)CLOCKS_PER_SEC);
starter=clock();
#endif
    
    return 0;
}


/**
* file = Snap-2.3/myApps/create-itemgraph/MyApp.cpp
* author = zhaoyi
*/

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <math.h>

#include<pthread.h>

#include "Snap.h"

using namespace std;

#define STEP 200
#define FEATURES 500

pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;

vector<struct node*> ls;
PUNGraph g;

double threshold=0.9;
int workers=1;
int features=20;
int itemcount=0;

struct node{
    double line[FEATURES];
};

struct tothread{
    int s,t;
};

int readLine(struct node *nn){
    int x=0;
    char ch, str[1024];
    while(scanf("%s",str)==1){
        nn->line[x++]=atof(str);
        scanf("%c",&ch);
        if(ch=='\n') break;
        scanf("%c",&ch);
        if(ch=='\n') break;
    }
    return x;
}

double cmp(struct node *a,struct node *b){
    double top=0, b1=0,b2=0;
    for(int i=0;i<features;i++){
        top+=a->line[i]*b->line[i];
        b1+=a->line[i]*a->line[i];
        b2+=b->line[i]*b->line[i];
    }
    return top/sqrt(b1*b2);
}

void *thr_fn(void *arg){
    struct tothread tt=*((struct tothread*)arg);
    
    for(int i=tt.s;i<tt.t;i++){
        if(i%STEP==0) printf("Prograss: %5d/%5d\t%f\%\n",i,tt.t,100.0*(double)(i-tt.s)/(double)(tt.t-tt.s));
        for(int j=i+1;j<itemcount;j++){
            double weight=cmp(ls[i],ls[j]);
            if(weight>threshold){
                pthread_mutex_lock(&mutex);
                g->AddEdge(i,j);
                pthread_mutex_unlock(&mutex);
            }
        }
    }
    
    return NULL;
}

void cut(int *x){
    for(int i=0;i<=workers;i++){
        double y=1.0-sqrt(((double)workers-(double)i)/(double)workers);
        x[i]=(int)(y*itemcount);
    }
}

int main(int args, char *argv[]){
    for(int i=1;i<args;i++) if(argv[i][0]=='-'){
        if(argv[i][1]=='f') features=atoi(argv[i]+2);
        if(argv[i][1]=='t') threshold=atof(argv[i]+2);
        if(argv[i][1]=='w') workers=atoi(argv[i]+2);
    }

    struct node *newnode=(struct node*)malloc(sizeof(struct node));
    
    while(readLine(newnode)>0){
        ls.push_back(newnode);
        newnode=(struct node*)malloc(sizeof(struct node));
        itemcount++;
    }
    printf("%d\n",itemcount);

    // Create new undirected
    g = TUNGraph::New();
    
    // Add nodes to graph
    for(int i=0;i<itemcount;i++){
        g->AddNode(i);
    }tothread tt;
    
    int x[workers+1];
    cut(x);
    
    pthread_t ps[workers];
    
    for(int i=0;i<workers;i++){
        tothread *tt=(tothread*)malloc(sizeof(tothread));
        tt->s=x[i];
        tt->t=x[i+1];
        pthread_create(ps+i,NULL,thr_fn,(void*)tt);
    }
    
    for(int i=0;i<workers;i++){
        pthread_join(ps[i],NULL);
    }
    
    printf("Saving to binary file... \n");
    
    TFOut FOut("./tmp/cpp.test.graph");
    g->Save(FOut);
    
    printf("------------------------------\nFinished\n");
    
    return 0;
}



wget http://snap.stanford.edu/releases/Snap-2.3.zip
unzip Snap-2.3.zip
rm Snap-2.3.zip

make snap
make

mkdir tmp
mkdir tmp/pcf

python setup/check.py

if [ "$?" -ne "0" ]; then
    # hyphen dash
    echo "------------------------------------------------------------"
    echo "Error! Stoped. "
    echo "Error(s) are displayed above. "
else
    python setup/setup.py
    for i in $(cat environ.conf); do export $i; done
    python filter.py -i$RIB_RAW_DATA_PATH -m5
    python jsonDumps.py -dtmp/filtedDataset.txt
fi


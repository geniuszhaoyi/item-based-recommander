# Usage : Reading information from FOOD.txt 

import sys
import os
import shutil
import logging
from gensim import corpora, models, similarities

import json


def main():
    Inputfile = './tmp/foods.train.txt'
    StoreJsonPath='tmp/json.txt'
    ItemListPath = 'tmp/itemlist.txt'
    
    f=open(ItemListPath)
    ls=f.readlines()
    itemlist={ls[i][:-1]:i for i in range(len(ls))}
    f.close()

    f=open(Inputfile)
        
    ds=[]
    d={}
    while True:
        l=f.readline()
        if not l: break
        td=json.loads(l)
        if 'itemId' not in d or d['itemId']!=itemlist[td['itemId']]:
            if 'itemId' in d: ds.append(d)
            d={}
            d['itemId']=itemlist[td['itemId']]
            d['review']=[]
        d['review'].append({'userId':td['userId']})
    if 'itemId' in d: ds.append(d)
    f.close()
    
    fout=open(StoreJsonPath,'w')
    #fout.write(json.dumps(ds, indent=2))
    fout.write(json.dumps(ds))
    fout.close()

if __name__=='__main__':
    main()

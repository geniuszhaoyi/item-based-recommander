# file = getPredicts.py
# author = zhaoyi

import os,sys

import json
import snap

import config

def getnbrs(g, i, depth, ans):
    if depth<=0: return 
    ni=g.GetNI(i)
    nns=ni.GetDeg()
    for nni in xrange(nns):
        thisnbr=ni.GetNbrNId(nni)
        if thisnbr not in ans:
            ans[thisnbr]=1
            getnbrs(g,thisnbr,depth-1,ans)

def neighborhoods(g, arr_itembought):
    NEIGHBOR_DIS = int(config.s['NEIGHBOR_DIS'])
    arrdict={a:1 for a in arr_itembought}
    ans={}
    
    for a in arr_itembought:
        recValue=0
        ni=g.GetNI(a)
        nns=ni.GetDeg()
        for nni in xrange(nns):
            thisnbr=ni.GetNbrNId(nni)
            if thisnbr in arrdict:
                recValue+=1
        if recValue<int(config.s['Rec_Value_Min']): continue
        getnbrs(g,a,NEIGHBOR_DIS,ans)
    
    return [key for key,val in ans.items() if key not in arrdict]

def main():
    NUM_ITEMS = 1363080
    MAX_REC = int(config.s['Recommendation_Max'])
    eps=0.00001
    
    # Read in RandomWalk Global Result 
    f=open(config.s['RandomWalk_Result_Globe_Path'])
    ls=f.readlines()
    NUM_ITEMS=int(ls[0])
    rwed=[0.00 for i in range(NUM_ITEMS)]
    for l in ls[1:]:
        x=l.split()
        rwed[int(x[0])]=float(x[1])
    f.close()
    desent_rwed=[(i,-rwed[i]) for i in range(len(rwed))]
    desent_rwed=sorted(desent_rwed, key=lambda desent_rwed:desent_rwed[1])
    
    # Read in Graph
    FIn = snap.TFIn(config.s['RandomWalk_Graph_Path'])
    g=snap.TUNGraph.Load(FIn)
    
    # Read in Json
    f=open(config.s['Json_Path'])
    strall=f.read()
    f.close()
    ds=json.loads(strall)
    
    users={}
    for d in ds:
        itemId=d['itemId']
        review=d['review']
        for r in review:
            userId=r['userId']
            if userId not in users: users[userId]=[]
            users[userId].append(itemId)
    
    bfstr=''
    
    # Read in ItemList
    fil=open(config.s['Item_List_Path'])
    itemlist=fil.readlines()
    fil.close()
    int2item={}
    for i in xrange(len(itemlist)):
        int2item[i]=itemlist[i][:-1]
    
    remain=len(users)
    for (u,ui) in users.items():
        if remain%1000==0: print remain
        remain-=1
        bfstr+=str(u)
        ns=neighborhoods(g,ui)
        recsl=[(n,rwed[n],0-rwed[n]) for n in ns]
        recsl=sorted(recsl, key=lambda recsl:recsl[2])
        lenrecsl=len(recsl)
        for i in xrange(min(MAX_REC,lenrecsl)):
            bfstr+=' '+str(int2item[recsl[i][0]])
        bfstr+='\n'
        
    frec=open(config.s['Result_Path'],'w')
    frec.write(bfstr)
    frec.close()

if __name__=='__main__':
    main()
